# data

Data files used by the scikit-image project

`Normal_Epidermis_and_Dermis_with_Intradermal_Nevus_10x.JPG`

- origin: https://en.wikipedia.org/wiki/File:Normal_Epidermis_and_Dermis_with_Intradermal_Nevus_10x.JPG
- license: public domain
- description: hematoxylin and eosin stained slide at 10x of normal epidermis and dermis with a benign intradermal nevus

`AS_09125_050116030001_D03f00d0.tif`

- origin: https://github.com/CellProfiler/examples/blob/master/ExampleHuman/images/AS_09125_050116030001_D03f00d0.tif
- license: CC0
- description: microscopy image of human cells provided by Jason Moffat
through [CellProfiler](https://cellprofiler.org/examples/#human-cells):
Moffat J, Grueneberg DA, Yang X, Kim SY, Kloepfer AM, Hinkle G, Piqani
B, Eisenhaure TM, Luo B, Grenier JK, Carpenter AE, Foo SY, Stewart SA,
Stockwell BR, Hacohen N, Hahn WC, Lander ES, Sabatini DM, Root DE
(2006) "A lentiviral RNAi library for human and mouse genes applied to
an arrayed viral high-content screen" Cell, 124(6):1283–98.
PMID: 16564017
:DOI:`10.1016/j.cell.2006.01.040`

`kidney-tissue-fluorescence.tif`

- origin: Image acquired by Genevieve Buckley at Monasoh Micro Imaging in 2018.
- license: CC0
- description: Mouse kidney tissue on a pre-prepared slide imaged with confocal fluorescence microscopy (Nikon C1 inverted microscope). Image shape is (16, 512, 512, 3). That is 512x512 pixels in X-Y, 16 image slices in Z, and 3 color channels (emission wavelengths 450nm, 515nm, and 605nm, respectively). Real space voxel size is 1.24 microns in X-Y, and 1.25 microns in Z. Data type is unsigned 16-bit integers.

`lily-of-the-valley-fluorescence.tif`

- origin: Image acquired by Genevieve Buckley at Monasoh Micro Imaging in 2018.
- license: CC0
- description: Lily of the valley plant stem on a pre-prepared slide imaged with confocal fluorescence microscopy (Nikon C1 inverted microscope). Image shape is (922, 922, 4). That is 922x922 pixels in X-Y, with 4 color channels. Real space voxel size is 1.24 microns in X-Y. Data type is unsigned 16-bit integers.

`astronaut_rl.npy`

- description: testdata for [skimage/restoration/tests/test_restoration](https://github.com/scikit-image/scikit-image/blob/master/skimage/restoration/tests/test_restoration.py)

`brain.tiff`

- description: Image shape is (10, 256, 256). That is 256x256 pixels in X-Y,
and 10 slices in Z.

`eagle.png`

- origin: Image acquired by Dayane Machado at the Prague Castle in 2019.
- license: CC0
- description: Golden eagle. Image shape is (2019, 1826).

`cells3d.tif`

- origin: Allen Institute for Cell Science
- licence: Unknown
- description: 3D fluorescence image of a monolayer of cells, including both
  membrane and nuclear stains. Image shape is (60, 2, 256, 256). The axes are
  in ZCYX order.

`pivchallenge/B/B001_{1,2}.tif`

- origin: Particle Image Velocimetry (PIV) Challenge http://pivchallenge.org
- license: CC0
- description: Case B1 image pair from the first PIV challenge. Synthetic
  strong vortex images with high particle density and small particles. See:
  http://pivchallenge.org/pub/index.html#b. Released as CC0 by Prof. Koji
  Okamoto and Prof. Jun Sakakibara.

`ridge-directed-ring-detector/Afik_20130217_img_{4900..5040..35}.png`

- origin: Images acquired by Eldad Afik at Victor Steinberg's lab, Department of Physics of Complex Systems, Weizmann Institute of Science, in 2013. Full dataset: https://doi.org/10.6084/m9.figshare.5119804
- license: CC0
- description: Out-of-focus microscopy imaging of 1um fluorescent passive tracers tracking an Elastic Turbulence flow in microfluidic. The algorithm and the data are presented in the paper: Afik (2015). https://doi.org/10.1038/srep13584.

`NPCsingleNucleus.tif`

- origin: https://git.embl.de/grp-ellenberg/boni_jcb_2015_livenpc/-/raw/main/example_images/NPCsingleNucleus.tif
- license: CC0
- description: microscopy images of human cells provided by Andrea Boni and
Jan Ellenberg. The image dataset has shape (15, 2, 180, 183), that is, 15
frames (time points), 2 channels, and 180x183 pixels in X-Y. For reference,
see paper: Boni A, Politi AZ, Strnad P, Xiang W, Hossain MJ, Ellenberg J (2015)
"Live imaging and modeling of inner nuclear membrane targeting reveals its
molecular requirements in mammalian cells" J Cell Biol 209(5):705–720.
ISSN: 0021-9525
:DOI:`10.1083/jcb.201409133`

`nickel_solidification.tif`

- origin: Collection of images acquired by C. Gus Becker and other researchers from Colorado School of Mines (CSM) and University of California, Santa Barbara (UCSB) at beamline 32-ID-B of the Advanced Photon Source (APS) at Argonne National Laboratory (ANL) in 2019.
- license: CC0
- description: Rapid solidification of a nickel alloy sample captured at 80,000 fps using high-speed transmission x-ray microscopy (TXM). The dataset is a timeseries of 11 images, each of shape (384, 512).
